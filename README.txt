Supplementary data refering to the paper 

"Footprints of domestication in dry-cured meat Penicillium fungi: convergent specific phenotypes and horizontal gene transfers"

Ying-Chu Lo†1, Jade Bruxaux†1, Ricardo C Rodríguez de la Vega1, Alodie Snirc1, Monika Coton2, Mélanie Le Piver3, Stéphanie Le Prieur1, Daniel Roueyre3, Joëlle Dupont4, Jos Houbraken5, Robert Debuchy6, Jeanne Ropars1, Tatiana Giraud* 1 and Antoine Branca* 1

1 Ecologie Systématique et Evolution, CNRS, AgroParisTech, Université Paris-Saclay, 91400 Orsay, France
2 Univ Brest, Laboratoire Universitaire de Biodiversité et Ecologie Microbienne, 29280 Plouzané, France
3 Laboratoire Interprofessionnel de Production - SAS L.I.P, 34 Rue de Salers, 15000 Aurillac, France
4 Origine, Structure, Evolution de la Biodiversité, UMR 7205 CNRS-MNHN, Muséum National d’Histoire Naturelle, CP39, 57 rue Cuvier, 75231 Paris Cedex 05, France
5 Westerdijk Fungal Biodiversity Institute, Uppsalalaan 8, 3584 CT Utrecht, the Netherlands
6 Université Paris-Saclay, CEA, CNRS, Institute for Integrative Biology of the Cell (I2BC), 91198, Gif-sur-Yvette, France. 

†These authors contributed equally
*These authors jointly supervised the study
Corresponding author: Jade Bruxaux bruxaux.jade@gmail.com

Files description:

HTR_not-masked.fasta -> fasta file containing the nucleotide sequences of the horizontally-transfered regions detected between Penicillium nalgiovense and P. salamii, before masking repeated regions.
HTR_TE-masked.fasta -> fasta file containing the nucleotide sequences of the horizontally-transfered regions detected between Penicillium nalgiovense and P. salamii, after masking repeated regions firmly identified as transposable elements (belonging to a transposable element family that could be identified).
HTR_repeat-masked.fasta -> fasta file containing the nucleotide sequences of the horizontally-transfered regions detected between Penicillium nalgiovense and P. salamii, after masking repeated regions identified as transposable elements but not always belonging to a transposable element family that could be identified.
HTR_not-masked.gff3 -> gff3 file containing the genes positions identified in the non-masked horizontally-transfered regions.
HTR_not-masked.emapper.annotations -> annotation file containing the putative functions of some genes identified in the non-masked horizontally-transfered regions.